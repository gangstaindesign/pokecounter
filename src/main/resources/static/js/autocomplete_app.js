$('#autocomplete').devbridgeAutocomplete({
    serviceUrl: '/suggest/',
    paramName: 'pokemonName'
});

$( "#basic-addon1" ).click(function() {
    if ($("#autocomplete").val()) {
        window.location.href = "/counters/" + $("#autocomplete").val();
    }
});

$("#autocomplete").focus();

$("#autocomplete").keypress(function(event) {
    if (event.which == 13) {
        event.preventDefault();
        $("#basic-addon1").click();
    }
});



/*$(window).resize(function() {
    if ($(this).width() < 768) {

        $('#search-again-button').show();
        $('.search-again').hide();
        $('#toggler').removeClass('glyphicon-remove');
        $('#toggler').addClass('glyphicon-search');
    } else {

        $('#search-again-button').hide();
        $('.search-again').show();

    }
});*/


$('#search-again-button').click(function(){
    $('.search-again').slideToggle(300);
    $('#toggler').toggleClass('glyphicon-search glyphicon-remove');
});



$( window ).load(function() {
    if($(window).width() < 768){

        $('#search-again-button').show();
        $('.search-again').hide();
        $('#toggler').removeClass('glyphicon-remove');
        $('#toggler').addClass('glyphicon-search');
    }
});
