package pl.pokecounter.cms.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.pokemon.domain.TypesRelation;

import java.util.List;

@Repository
public interface TypesRelationDao extends JpaRepository<TypesRelation, Long> {
    List<TypesRelation> findByDefendingTypeId(Long elementTypeId);
}
