package pl.pokecounter.cms.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.pokecounter.frontpage.domain.ContactMessage;

import java.util.List;

@Repository
public interface MailDao extends JpaRepository<ContactMessage, Long> {

    @Query(value = "SELECT * FROM contact_messages  LIMIT 10", nativeQuery = true)
    List<ContactMessage> findTop10();
}
