package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.pokecounter.frontpage.service.FaqService;
import pl.pokecounter.frontpage.domain.Faq;

@Controller
@RequestMapping("/admin/faq")
public class FaqCmsController {

    @Autowired FaqService faqService;

    @RequestMapping("")
    public String home(Model model) {
        model.addAttribute("faqs", faqService.findAll());
        return "cms/faq/faq_crud";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model){
        model.addAttribute("faq", new Faq());
        return "cms/faq/faq_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSubmit(@ModelAttribute Faq faq) {
        faqService.save(faq);
        return "redirect:/admin/faq";
    }

    @RequestMapping(value = "/delete/{id}", method = RequestMethod.GET)
    public String delete(@PathVariable Long id) {
        faqService.delete(id);
        return "redirect:/admin/faq";
    }

    @RequestMapping(value = "/update/{id}", method = RequestMethod.GET)
    public String edit(@PathVariable Long id, Model model) {
        model.addAttribute("faq", faqService.findById(id));
        return "cms/faq/faq_update";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String edit(@ModelAttribute Faq faq){
        faqService.update(faq);
        return "redirect:/admin/faq";
    }

}
