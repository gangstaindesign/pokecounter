package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import pl.pokecounter.cms.service.TypeCmsService;
import pl.pokecounter.pokemon.domain.ElementType;

@Controller
@RequestMapping("/admin/types")
public class ElementCmsController {

    @Autowired private TypeCmsService typeCmsService;

    @RequestMapping("")
    public String listAll(Model model){
        model.addAttribute("types", typeCmsService.findAll());
        return "cms/type_crud";
    }

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(Model model) {
        model.addAttribute("type", new ElementType());
        return "cms/type_add";
    }

    @RequestMapping(value="/add", method = RequestMethod.POST)
    public String addSubmit(@ModelAttribute ElementType type){
        typeCmsService.save(type);
        return "redirect:/admin/types";
    }

    @RequestMapping(value = "/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        ElementType element = typeCmsService.findOne(id);
        model.addAttribute("type", element);
        model.addAttribute("counters", typeCmsService.findCounters(id));
        return "cms/type_update";
    }

    @RequestMapping(value="/update", method = RequestMethod.POST)
    public String updateSubmit(@ModelAttribute ElementType type){
        typeCmsService.update(type);
        return "redirect:/admin/types";
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable Long id) {
        typeCmsService.delete(id);
        return "redirect:/admin/types";
    }

}
