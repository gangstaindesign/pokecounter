package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.pokecounter.cms.service.TypeCmsService;
import pl.pokecounter.cms.service.PokemonCmsService;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;

@Controller
@RequestMapping("/admin/pokemons/types")
public class PokemonTypeCmsController {

    @Autowired PokemonCmsService pokemonCmsService;
    @Autowired TypeCmsService typeCmsService;

    @RequestMapping("")
    public String listAll(@RequestParam Long pk_id, Model model){
        model.addAttribute("pokemon", pokemonCmsService.findOne(pk_id));
        model.addAttribute("types", pokemonCmsService.findOne(pk_id).getElementTypes());
        return "cms/pokemon_types_crud";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(@RequestParam Long pk_id, Model model){
        model.addAttribute("newType", new ElementType());
        model.addAttribute("types", typeCmsService.findAll());
        model.addAttribute("pokemon", pokemonCmsService.findOne(pk_id));
        return "cms/pokemon_types_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSubmit(@RequestParam Long pk_id, @ModelAttribute Pokemon pokemon) {
        pokemonCmsService.addElement(pokemon);
        return "redirect:/admin/pokemons/types?pk_id="+pk_id;
    }

    @RequestMapping(value = "/delete")
    public String delete(@RequestParam Long pk_id, @RequestParam Long type_id) {
        pokemonCmsService.deleteType(pk_id,type_id);
        return "redirect:/admin/pokemons/types?pk_id="+pk_id;
    }


}
