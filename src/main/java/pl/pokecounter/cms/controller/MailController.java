package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import pl.pokecounter.cms.service.MailService;

@Controller
@RequestMapping("/admin/mail")
public class MailController {

    @Autowired MailService mailService;

    @RequestMapping("")
    public String home(Model model){
        model.addAttribute("mails", mailService.findLatest10());
        return "cms/mail/mail_home";
    }

    @RequestMapping("/delete/{id}")
    public String delete(@PathVariable Long id) {
        mailService.delete(id);
        return "redirect:/admin/mail";
    }
}
