package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.pokecounter.cms.service.TypeCmsService;
import pl.pokecounter.cms.service.PokemonCmsService;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;

import java.util.stream.Collectors;

@Controller
@RequestMapping("/admin/pokemons")
public class PokemonCmsController {

    @Autowired private PokemonCmsService pokemonCmsService;
    @Autowired private TypeCmsService typeCmsService;


    @RequestMapping("")
    public String listAll(Model model){
        model.addAttribute("pokemons", pokemonCmsService.findAllOrderByPokedexIdAsc());
        return "cms/pokemon_crud";
    }

    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(Model model){
        model.addAttribute("pokemon", new Pokemon());
        model.addAttribute("types", typeCmsService.findAll());
        return "cms/pokemon_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSumbit(@ModelAttribute Pokemon pokemon){
        pokemonCmsService.save(pokemon);
        return "redirect:/admin/pokemons/details/"+pokemon.getId();
    }

    @RequestMapping(value = "/delete/{id}")
    public String delete(@PathVariable Long id) {
        Pokemon pokemon = pokemonCmsService.findOne(id);
        Iterable<ElementType> all_types = typeCmsService.findAll();
        for (ElementType type : all_types) {
            type.setPokemons(type.getPokemons()
                    .stream()
                    .filter(x -> x.getId() != pokemon.getId())
                    .collect(Collectors.toList()));
            typeCmsService.save(type);
        }
        pokemonCmsService.delete(id);
        return "redirect:/admin/pokemons";
    }

    @RequestMapping(value = "/update/{id}")
    public String update(@PathVariable Long id, Model model) {
        model.addAttribute("pokemon", pokemonCmsService.findOne(id));
        return "cms/pokemon_update";
    }

    @RequestMapping(value="/update", method = RequestMethod.POST)
    public String updateSubmit(@ModelAttribute Pokemon pokemon){
        pokemonCmsService.update(pokemon);
        return "redirect:/admin/pokemons/details/"+pokemon.getId();
    }

    @RequestMapping(value = "/details/{id}")
    public String details(@PathVariable Long id, Model model) {
        model.addAttribute("pokemon", pokemonCmsService.findOne(id));
        return "cms/pokemon_details";
    }

}
