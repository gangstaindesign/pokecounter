package pl.pokecounter.cms.controller;


import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class HomeCmsController {

    @RequestMapping("/admin")
    public String home(){
        return "cms/home";
    }

}
