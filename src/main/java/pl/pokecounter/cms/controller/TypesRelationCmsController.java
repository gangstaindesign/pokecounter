package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import pl.pokecounter.cms.service.TypeCmsService;
import pl.pokecounter.cms.service.TypesRelationCmsService;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.TypesRelation;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/admin/relations")
public class TypesRelationCmsController {

    @Autowired private TypeCmsService typeCmsService;
    @Autowired private TypesRelationCmsService typesRelationCmsService;


    @RequestMapping(value = "/add", method = RequestMethod.GET)
    public String add(@RequestParam Long type_id, Model model){
        ElementType baseType = typeCmsService.findOne(type_id);
        if (baseType.getTypesRelations().size() == 0) {
            List<TypesRelation> relations = new ArrayList<TypesRelation>();

            for (ElementType elementType : typeCmsService.findAll()) {
                TypesRelation relation = new TypesRelation();
                relation.setDefendingType(baseType);
                relation.setAttackingType(elementType);
                relation.setDamageMultiplier(1d);
                relations.add(relation);
            }

            baseType.setTypesRelations(relations);
        }
        model.addAttribute("baseType", baseType);
        return "cms/types_relations_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSubmit(@ModelAttribute ElementType type) {
        typesRelationCmsService.save(type);
        return "redirect:/admin/types/update/"+type.getId();
    }

    @RequestMapping(value = "/delete")
    public String delete(@RequestParam Long ctr_id){
        Long typeId = typesRelationCmsService.findTypeId(ctr_id);
        typesRelationCmsService.delete(ctr_id);
        return "redirect:/admin/types/update/"+typeId;
    }

}
