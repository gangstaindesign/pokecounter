package pl.pokecounter.cms.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;
import pl.pokecounter.cms.service.TypeCmsService;
import pl.pokecounter.cms.service.PokemonAttackCmsService;
import pl.pokecounter.cms.service.PokemonCmsService;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.PokemonAttack;

@Controller
@RequestMapping("/admin/attacks")
public class PokemonAttackCmsController {

    @Autowired PokemonCmsService pokemonCmsService;
    @Autowired PokemonAttackCmsService pokemonAttackCmsService;
    @Autowired TypeCmsService typeCmsService;

    @RequestMapping("")
    public String listAttacks(@RequestParam Long pk_id, Model model) {
        Pokemon pokemon = pokemonCmsService.findOne(pk_id);
        model.addAttribute("attacks", pokemon.getPokemonAttacks());
        model.addAttribute("pokemon", pokemon);
        return "cms/attack_crud";
    }

    @RequestMapping(value="/add", method = RequestMethod.GET)
    public String add(@RequestParam Long pk_id, Model model) {
        PokemonAttack attack = new PokemonAttack();
        model.addAttribute("attack", attack);
        model.addAttribute("pokemon", pokemonCmsService.findOne(pk_id));
        model.addAttribute("types", typeCmsService.findAll());
        return "cms/attack_add";
    }

    @RequestMapping(value = "/add", method = RequestMethod.POST)
    public String addSubmit(@ModelAttribute PokemonAttack pokemonAttack, @RequestParam Long pk_id) {
        pokemonAttackCmsService.save(pokemonAttack, pk_id);
        return "redirect:/admin/attacks?pk_id="+pk_id;
    }

    @RequestMapping(value = "/delete")
    public String delete(@RequestParam Long pk_id, @RequestParam Long atk_id) {
        pokemonAttackCmsService.delete(atk_id);
        return "redirect:/admin/attacks?pk_id="+pk_id;
    }

    @RequestMapping(value = "/update", method = RequestMethod.GET)
    public String update(@RequestParam Long pk_id, @RequestParam Long atk_id, Model model) {
        PokemonAttack attack = pokemonAttackCmsService.findOne(atk_id);
        Pokemon pokemon = pokemonCmsService.findOne(pk_id);
        model.addAttribute("attack", attack);
        model.addAttribute("pokemon", pokemon);
        model.addAttribute("types", typeCmsService.findAll());
        return "cms/attack_update";
    }

    @RequestMapping(value = "/update", method = RequestMethod.POST)
    public String update(@RequestParam Long pk_id, @ModelAttribute PokemonAttack pokemonAttack) {
        pokemonAttackCmsService.update(pokemonAttack);
        return "redirect:/admin/attacks?pk_id="+pk_id;
    }

}
