package pl.pokecounter.cms.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pokecounter.pokemon.dao.PokemonAttackDao;
import pl.pokecounter.pokemon.dao.PokemonDao;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.PokemonAttack;

import java.util.List;

@Service
public class PokemonAttackCmsService {

    @Autowired PokemonAttackDao pokemonAttackDao;
    @Autowired PokemonDao pokemonDao;

    @Transactional
    public void save(PokemonAttack pokemonAttack, Long pk_id) {
        Pokemon pokemon =  pokemonDao.findOne(pk_id);

        pokemonAttack.setPokemonWithThisAttack(pokemon);
        pokemon.getPokemonAttacks().add(pokemonAttack);

        pokemonAttackDao.save(pokemonAttack);
        pokemonDao.save(pokemon);
    }

    public void delete(Long atk_id) {
        pokemonAttackDao.delete(atk_id);
    }

    public PokemonAttack findOne(Long atk_id) {
        return pokemonAttackDao.findOne(atk_id);
    }

    public void update(PokemonAttack pokemonAttack) {
        PokemonAttack pokemonToUpdate = pokemonAttackDao.findOne(pokemonAttack.getId());
        copyFromTo(pokemonAttack, pokemonToUpdate);
        pokemonAttackDao.saveAndFlush(pokemonToUpdate);
    }

    private void copyFromTo(PokemonAttack pokemonAttack, PokemonAttack old) {
        old.setName(pokemonAttack.getName());
        old.setElementType(pokemonAttack.getElementType());
        old.setAttackType(pokemonAttack.getAttackType());
    }
}
