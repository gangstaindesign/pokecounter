package pl.pokecounter.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.cms.dao.TypeDao;
import pl.pokecounter.pokemon.dao.PokemonDao;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class PokemonCmsService {

    @Autowired private PokemonDao pokemonDao;
    @Autowired private TypeDao typeDao;

    public Pokemon findOne(Long id) {
        return pokemonDao.findOne(id);
    }

    @Transactional
    public void save(Pokemon pokemon) {
        pokemonDao.save(pokemon);
    }

    @Transactional
    public void delete(Long id) {
        pokemonDao.delete(id);
    }

    @Transactional
    public void update(Pokemon pokemon) {
        Pokemon pokemonToUpdate = pokemonDao.findOne(pokemon.getId());
        copyFromTo(pokemon, pokemonToUpdate);
        pokemonDao.saveAndFlush(pokemonToUpdate);
    }

    @Transactional
    public void addElement(Pokemon pokemon) {
        Pokemon old = pokemonDao.findOne(pokemon.getId());
        ElementType type = pokemon.getElementTypes().get(0);

        type.getPokemons().add(old);
        old.getElementTypes().add(type);

        pokemonDao.saveAndFlush(old);
        typeDao.saveAndFlush(type);
    }

    public void deleteType(Long pk_id, Long type_id) {
        Pokemon pokemon = pokemonDao.findOne(pk_id);
        List<ElementType> elements = pokemon.getElementTypes().stream()
                .filter(x -> x.getId() != type_id)
                .collect(Collectors.toList());
        pokemon.setElementTypes(elements);

        ElementType type = typeDao.findOne(type_id);
        List<Pokemon> pokemons = type.getPokemons().stream()
                .filter(x -> x.getId() != pk_id)
                .collect(Collectors.toList());
        type.setPokemons(pokemons);

        pokemonDao.saveAndFlush(pokemon);
        typeDao.saveAndFlush(type);
    }

    public Iterable<Pokemon> findAllOrderByPokedexIdAsc() {
        return pokemonDao.findAll()
                .stream()
                .sorted((x1, x2) -> Integer.compare(Integer.parseInt(x1.getPokedexId().substring(1)),
                        Integer.parseInt(x2.getPokedexId().substring(1)))).collect(Collectors.toList());
    }

    private void copyFromTo(Pokemon pokemon, Pokemon pokemonToUpdate) {
        pokemonToUpdate.setPokedexId(pokemon.getPokedexId());
        pokemonToUpdate.setName(pokemon.getName());
        pokemonToUpdate.setImageUrl(pokemon.getImageUrl());
        pokemonToUpdate.setDescription(pokemon.getDescription());
        pokemonToUpdate.setRarity(pokemon.getRarity());
    }
}
