package pl.pokecounter.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.cms.dao.MailDao;
import pl.pokecounter.frontpage.domain.ContactMessage;

import java.util.List;

@Service
public class MailService {

    @Autowired MailDao mailDao;


    public List<ContactMessage> findLatest10() {
        return mailDao.findTop10();
    }

    public void delete(Long id) {
        mailDao.delete(id);
    }
}
