package pl.pokecounter.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.cms.dao.TypeDao;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.TypesRelation;

import javax.transaction.Transactional;
import java.util.List;
import java.util.stream.Collectors;

@Service
public class TypeCmsService {

    @Autowired private TypeDao typeDao;
    @Autowired private TypesRelationCmsService typesRelationCmsService;

    public ElementType findOne(Long id) {
        return typeDao.findOne(id);
    }

    public List<ElementType> findAll() {
        return typeDao.findAll();
    }

    @Transactional
    public void save(ElementType elementType) {
        typeDao.save(elementType);
    }

    @Transactional
    public void delete(Long id) {
        deleteTypeRelations(id);
        typeDao.delete(id);
    }

    @Transactional
    public void update(ElementType elementType) {
        ElementType typeToUpdate = typeDao.findOne(elementType.getId());
        typeToUpdate.setName(elementType.getName());
        typeToUpdate.setColor(elementType.getColor());
        typeDao.saveAndFlush(typeToUpdate);
    }

    public List<TypesRelation> findCounters(Long id) {
        ElementType type = typeDao.findOne(id);
        return type.getTypesRelations()
                .stream().filter(x -> x.getDamageMultiplier() > 1)
                .collect(Collectors.toList());
    }

    private void deleteTypeRelations(Long id) {
        List<Long> ids = typesRelationCmsService.findAll().stream()
                .filter(x -> x.getDefendingType().getId() == id)
                .map(x -> x.getId())
                .collect(Collectors.toList());
        ids.stream().forEach(x -> typesRelationCmsService.delete(x));
    }
}
