package pl.pokecounter.cms.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pokecounter.cms.dao.TypeDao;
import pl.pokecounter.cms.dao.TypesRelationDao;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.TypesRelation;

import java.util.List;

@Service
public class TypesRelationCmsService {

    @Autowired private TypesRelationDao typesRelationDao;
    @Autowired private TypeDao typeDao;

    public List<TypesRelation> findAll() {
        return typesRelationDao.findAll();
    }

    public void save(TypesRelation relation) {
        ElementType type = typeDao.findOne(relation.getDefendingType().getId());
        type.getTypesRelations().add(relation);
        typesRelationDao.saveAndFlush(relation);
        typeDao.saveAndFlush(type);
    }

    public Long findTypeId(Long rel_id) {
        return typesRelationDao.findOne(rel_id).getDefendingType().getId();
    }

    public void delete(Long ctr_id) {
        typesRelationDao.delete(ctr_id);
    }

    @Transactional
    public void save(ElementType model) {
        ElementType baseType = typeDao.findOne(model.getId());

        typesRelationDao.delete(typesRelationDao.findByDefendingTypeId(baseType.getId()));

        for (TypesRelation relation : model.getTypesRelations()) {
            TypesRelation newRelation = createRelation(relation);
            baseType.getTypesRelations().add(newRelation);

            typesRelationDao.saveAndFlush(relation);
            typeDao.saveAndFlush(baseType);
        }

    }

    private TypesRelation createRelation(TypesRelation vun) {
        TypesRelation newVun = new TypesRelation();
        ElementType elementType = typeDao.findOne(vun.getDefendingType().getId());
        ElementType goodAgainst = typeDao.findOne(vun.getAttackingType().getId());
        newVun.setDefendingType(elementType);
        newVun.setAttackingType(goodAgainst);
        newVun.setDamageMultiplier(vun.getDamageMultiplier());
        return newVun;
    }
}
