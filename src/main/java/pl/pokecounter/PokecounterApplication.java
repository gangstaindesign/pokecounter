package pl.pokecounter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;

@SpringBootApplication
public class PokecounterApplication {

	public static void main(String[] args) {
		SpringApplication.run(PokecounterApplication.class, args);
	}

}
