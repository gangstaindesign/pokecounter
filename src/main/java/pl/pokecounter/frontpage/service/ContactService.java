package pl.pokecounter.frontpage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.frontpage.dao.ContactDao;
import pl.pokecounter.frontpage.domain.ContactMessage;

@Service
public class ContactService {

    @Autowired ContactDao contactDao;

    public void save(ContactMessage contactMessage) {
        contactDao.save(contactMessage);
    }

}

