package pl.pokecounter.frontpage.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.frontpage.dao.FaqDao;
import pl.pokecounter.frontpage.domain.Faq;

import java.util.List;

@Service
public class FaqService {

    @Autowired FaqDao faqDao;

    public void save(Faq faq) {
        faqDao.save(faq);
    }

    public List<Faq> findAll() {
        return faqDao.findAll();
    }

    public void delete(Long id) {
        faqDao.delete(id);
    }

    public Faq findById(Long id) {
        return faqDao.findOne(id);
    }

    public void update(Faq faq) {
        Faq faqToUpdate = faqDao.findOne(faq.getId());
        faqToUpdate.setQuestion(faq.getQuestion());
        faqToUpdate.setAnswer(faq.getAnswer());
        faqToUpdate.setFrequency(faq.getFrequency());
        faqDao.saveAndFlush(faqToUpdate);
    }
}
