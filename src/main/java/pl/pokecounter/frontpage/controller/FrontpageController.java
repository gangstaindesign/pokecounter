package pl.pokecounter.frontpage.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.view.RedirectView;
import pl.pokecounter.frontpage.domain.ContactMessage;
import pl.pokecounter.frontpage.service.ContactService;
import pl.pokecounter.frontpage.service.FaqService;
import pl.pokecounter.pokemon.service.PokemonSearchService;
import pl.pokecounter.pokemon.service.PokemonService;

@Controller
public class FrontpageController {

    @Autowired private PokemonSearchService pokemonSearchService;
    @Autowired private PokemonService pokemonService;
    @Autowired private FaqService faqService;
    @Autowired private ContactService contactService;

    @RequestMapping("/")
    public ModelAndView getHomepage(@RequestParam(required = false) Boolean error) {
        final ModelAndView mav = new ModelAndView("front/index");
        if (error != null && error) {
            mav.addObject("error", true);
        }
        mav.addObject("latestSearches", pokemonSearchService.findLatestFourPokemonSearches());
        mav.addObject("popularSearches", pokemonSearchService.findFourMostPopularPokemonSearches());
        return mav;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.GET)
    public ModelAndView getContactForm(@RequestParam(required = false) Boolean sent)  {
        final ModelAndView mav = new ModelAndView("front/contact");
        mav.addObject("contactMessage", new ContactMessage());
        if (sent != null && sent) {
            mav.addObject("sent", true);
        }
        return mav;
    }

    @RequestMapping(value = "/contact", method = RequestMethod.POST)
    public ModelAndView sendContactForm(@ModelAttribute ContactMessage contactMessage) {
        final ModelAndView mav = new ModelAndView(new RedirectView("/contact"));
        contactService.save(contactMessage);
        mav.addObject("sent", true);
        return mav;
    }

    @RequestMapping("/pokemon-list")
    public ModelAndView allPokemons() {
        final ModelAndView mav = new ModelAndView("front/pokemonlist");
        mav.addObject("pokemons", pokemonService.findAll());
        return mav;
    }

    @RequestMapping("/faq")
    public ModelAndView faq() {
        final ModelAndView mav = new ModelAndView("front/faq");
        mav.addObject("faqs", faqService.findAll());
        return mav;
    }

    @RequestMapping("/googleb542a8da5517962c.html")
    public String getGoogle() {
        return "front/googleb542a8da5517962c";
    }

}
