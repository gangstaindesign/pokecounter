package pl.pokecounter.frontpage.domain;

import pl.pokecounter.pokemon.domain.AbstractEntity;

import javax.persistence.Entity;
import javax.persistence.Id;

@Entity(name = "faq")
public class Faq extends AbstractEntity {

    private String question;
    private String answer;
    private int frequency;

    public int getFrequency() {
        return frequency;
    }

    public void setFrequency(int frequency) {
        this.frequency = frequency;
    }

    public String getQuestion() {
        return question;
    }

    public void setQuestion(String question) {
        this.question = question;
    }

    public String getAnswer() {
        return answer;
    }

    public void setAnswer(String answer) {
        this.answer = answer;
    }
}
