package pl.pokecounter.frontpage.domain;

import pl.pokecounter.pokemon.domain.AbstractEntity;

import javax.persistence.Column;
import javax.persistence.Entity;

@Entity(name = "contact_messages")
public class ContactMessage extends AbstractEntity {

    private String email;
    private String name;
    private String message;

    @Column
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Column
    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }
}
