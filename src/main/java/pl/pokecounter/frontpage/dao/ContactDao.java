package pl.pokecounter.frontpage.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.frontpage.domain.ContactMessage;

@Repository
public interface ContactDao extends JpaRepository<ContactMessage, Long>{
}
