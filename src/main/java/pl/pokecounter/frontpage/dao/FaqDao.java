package pl.pokecounter.frontpage.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.frontpage.domain.Faq;

@Repository
public interface FaqDao extends JpaRepository<Faq, Long> {
}
