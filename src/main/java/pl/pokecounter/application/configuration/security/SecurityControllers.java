package pl.pokecounter.application.configuration.security;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
public class SecurityControllers {

    @RequestMapping("/login")
    public String login() {
        return "login";
    }

}
