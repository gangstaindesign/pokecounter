package pl.pokecounter.application.configuration.urlrewrite;

import org.springframework.boot.context.embedded.FilterRegistrationBean;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.tuckey.web.filters.urlrewrite.UrlRewriteFilter;

@Configuration
public class UrlRewriteConfiguration {

    @Bean
    public FilterRegistrationBean getUrlRewriteFilter() {
        FilterRegistrationBean filterRegistrationBean = new FilterRegistrationBean();

        filterRegistrationBean.setFilter(new UrlRewriteFilter());
        filterRegistrationBean.addInitParameter("confPath", "urlrewrite.xml");

        return filterRegistrationBean;
    }

}
