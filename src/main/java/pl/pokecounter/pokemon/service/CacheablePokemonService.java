package pl.pokecounter.pokemon.service;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.cms.dao.TypeDao;
import pl.pokecounter.pokemon.dao.PokemonAttackDao;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.PokemonAttack;
import pl.pokecounter.pokemon.domain.TypesRelation;
import pl.pokecounter.pokemon.domain.dto.CounterDTO;
import pl.pokecounter.pokemon.domain.dto.CounterTypeFrontDTO;
import pl.pokecounter.pokemon.domain.dto.Effectiveness;
import pl.pokecounter.pokemon.domain.dto.PokemonFrontDTO;

import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;

@Service
public class CacheablePokemonService {

    @Autowired private PokemonAttackDao pokemonAttackDao;
    @Autowired private TypeDao typeDao;

    public List<PokemonFrontDTO> findPokemonCounters(Pokemon pokemon) {

        List<CounterDTO> counterTypesDTO = typeDao.findAll()
                .stream()
                 .map( type -> new CounterDTO(type, getDamageMultiplier(pokemon.getElementTypes(), type)))
                 .filter( type -> isCounter(type))
                .collect(toList());

        List<ElementType> counterTypes = counterTypesDTO
                .stream()
                .map(counterDTO -> counterDTO.getElementType())
                .collect(Collectors.toList());

        List<ElementType> doubleEffectiveTypes = counterTypesDTO
                .stream()
                .filter(counterDTO -> isDoubleEffective(counterDTO))
                .map(counterDTO -> counterDTO.getElementType())
                .collect(Collectors.toList());

        return pokemonAttackDao.findAllByElementTypeIn(counterTypes)
                .stream()
                .map(PokemonAttack::getPokemonWithThisAttack)
                .distinct()
                .map(poke -> PokemonFrontDTO.mapToDTOWithAttacks(poke, counterTypes))
                .sorted(comparing(PokemonFrontDTO::isAtleastHalfAttacksCounters ,reverseOrder())
                        .thenComparing(comparing(x -> x.getAttacksToTypesFittingAttacksRatio(doubleEffectiveTypes),reverseOrder()))
                        .thenComparing(comparing(PokemonFrontDTO::getActiveAttacksRatio, reverseOrder())))
                .collect(toList());
    }

    private boolean isDoubleEffective(CounterDTO counterDTO) {
        return counterDTO.getEffectiveness() == Effectiveness.DOUBLE_EFFECTIVE;
    }

    private boolean isCounter(CounterDTO type) {
        boolean effective = type.getEffectiveness() == Effectiveness.EFFECTIVE;
        boolean doublEffective = type.getEffectiveness() == Effectiveness.DOUBLE_EFFECTIVE;
        return effective || doublEffective;
    }

    private double getDamageMultiplier(List<ElementType> pokemonElementTypes, ElementType element) {
        return pokemonElementTypes.stream()
                .map(ElementType::getTypesRelations)
                .flatMap(Collection::stream)
                .filter(x -> x.getAttackingType() == element)
                .map(TypesRelation::getDamageMultiplier)
                .reduce((e1, e2) -> e1 * e2)
                .get();

    }
}
