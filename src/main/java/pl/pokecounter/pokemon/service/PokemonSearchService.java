package pl.pokecounter.pokemon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.pokemon.dao.LatestPokemonSearchDao;
import pl.pokecounter.pokemon.dao.PopularPokemonSearchDao;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.LatestPokemonSearch;
import pl.pokecounter.pokemon.domain.PopularPokemonSearch;

import javax.servlet.http.HttpServletRequest;
import javax.transaction.Transactional;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import static pl.pokecounter.pokemon.util.IpUtils.getIpAddress;

@Service
public class PokemonSearchService {

    @Autowired private LatestPokemonSearchDao latestPokemonSearchDao;
    @Autowired private PopularPokemonSearchDao popularPokemonSearchDao;

    @Transactional
    public void addNewEntry(Pokemon pokemon, HttpServletRequest httpServletRequest) {
        LatestPokemonSearch latestPokemonSearch = new LatestPokemonSearch();
        latestPokemonSearch.setPokemon(pokemon);
        latestPokemonSearch.setSearchedAt(new Date());
        latestPokemonSearch.setIpAddress(getIpAddress(httpServletRequest));
        latestPokemonSearchDao.save(latestPokemonSearch);
    }

    @Transactional
    public void addNewTick(Pokemon pokemon) {
        Optional<PopularPokemonSearch> pokemonSearchMaybe = popularPokemonSearchDao.findOneByPokemon(pokemon);

        if (!pokemonSearchMaybe.isPresent()) {
            PopularPokemonSearch pokemonSearch = new PopularPokemonSearch();
            pokemonSearch.setPokemon(pokemon);
            pokemonSearch.setTicks(0L);
            pokemonSearchMaybe = Optional.of(pokemonSearch);
        }

        PopularPokemonSearch actualPokemonSearch = pokemonSearchMaybe.get();

        actualPokemonSearch.setTicks(actualPokemonSearch.getTicks() + 1);
        popularPokemonSearchDao.saveAndFlush(actualPokemonSearch);
    }

    public List<LatestPokemonSearch> findLatestFourPokemonSearches() {
        return latestPokemonSearchDao.findDistinctFour();
    }

    public List<PopularPokemonSearch> findFourMostPopularPokemonSearches() {
        return popularPokemonSearchDao.findTop4ByOrderByTicksDesc();
    }

}
