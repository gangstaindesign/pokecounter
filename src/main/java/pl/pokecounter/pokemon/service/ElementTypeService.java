package pl.pokecounter.pokemon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import pl.pokecounter.cms.dao.TypeDao;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.TypesRelation;
import pl.pokecounter.pokemon.domain.dto.CounterTypeFrontDTO;

import java.util.ArrayList;
import java.util.Collection;
import java.util.List;
import java.util.stream.Collectors;

import static java.util.Comparator.comparing;
import static java.util.Comparator.reverseOrder;
import static java.util.stream.Collectors.toList;

@Service
public class ElementTypeService {
    @Autowired TypeDao typeDao;

    public List<CounterTypeFrontDTO> getAllCounterTypesForPokemon(Pokemon pokemon) {

        List<ElementType> pokemonElementTypes = pokemon.getElementTypes();
        
        if (pokemonElementTypes.size() == 2) {
            List<CounterTypeFrontDTO> counterTypes = new ArrayList<>();
            List<ElementType> allElements = typeDao.findAll();
            for (ElementType elementType : allElements) {
                TypesRelation vun1 = pokemonElementTypes.get(0).getTypesRelations()
                        .stream()
                        .filter(x -> x.getAttackingType() == elementType)
                        .findFirst()
                        .get();
                TypesRelation vun2 = pokemonElementTypes.get(1).getTypesRelations()
                        .stream().filter(x -> x.getAttackingType() == elementType)
                        .findFirst()
                        .get();
                if (vun1.getDamageMultiplier() * vun2.getDamageMultiplier() > 1.0) {
                    CounterTypeFrontDTO counterType = new CounterTypeFrontDTO(vun1.getAttackingType(),
                            vun1.getDamageMultiplier() * vun2.getDamageMultiplier());
                    counterTypes.add(counterType);
                }
            }

            return counterTypes.stream()
                    .sorted(comparing(CounterTypeFrontDTO::getEffectiveness,reverseOrder()))
                    .collect(Collectors.toList());
        }
        else{
            return pokemon.getElementTypes()
                    .stream()
                    .map(ElementType::getTypesRelations)
                    .flatMap(Collection::stream)
                    .filter(x -> x.getDamageMultiplier()>1)
                    .map(TypesRelation::getAttackingType)
                    .map(counterType -> new CounterTypeFrontDTO(counterType, 1.25))
                    .collect(toList());
        }


    }
}
