package pl.pokecounter.pokemon.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import pl.pokecounter.pokemon.dao.PokemonAttackDao;
import pl.pokecounter.pokemon.dao.PokemonDao;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.dto.PokemonFrontDTO;
import pl.pokecounter.pokemon.domain.dto.PokemonSuggestionDTO;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

import static java.util.stream.Collectors.toList;

@Service
public class PokemonService {

    @Autowired private PokemonDao pokemonDao;
    @Autowired private PokemonAttackDao pokemonAttackDao;
    @Autowired private PokemonSearchService pokemonSearchService;
    @Autowired private CacheablePokemonService cacheablePokemonService;

    public Iterable<Pokemon> findAll() {
        return pokemonDao.findAllByOrderByNameAsc();
    }

    public List<PokemonSuggestionDTO> findAllByNameLike(String name) {
        return pokemonDao.findByNameContainingIgnoreCase(name)
                .stream()
                .map(PokemonSuggestionDTO::new)
                .collect(toList());
    }

    public Optional<Pokemon> findByName(String name) {
        return pokemonDao.findOneByNameIgnoreCase(name);
    }

    @Transactional
    public List<PokemonFrontDTO> findAllCountersForPokemon(Pokemon pokemon, HttpServletRequest httpServletRequest) {
        pokemonSearchService.addNewEntry(pokemon, httpServletRequest);
        pokemonSearchService.addNewTick(pokemon);

        return cacheablePokemonService.findPokemonCounters(pokemon);
    }


}
