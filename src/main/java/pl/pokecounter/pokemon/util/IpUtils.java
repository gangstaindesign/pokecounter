package pl.pokecounter.pokemon.util;

import javax.servlet.http.HttpServletRequest;

public abstract class IpUtils {

    public static String getIpAddress(HttpServletRequest httpServletRequest) {
        String ipAddress = httpServletRequest.getHeader("X-FORWARDED-FOR");
        if (ipAddress == null) {
            ipAddress = httpServletRequest.getRemoteAddr();
        }
        return ipAddress;
    }

}
