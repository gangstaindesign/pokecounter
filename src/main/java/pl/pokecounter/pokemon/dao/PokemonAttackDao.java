package pl.pokecounter.pokemon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.PokemonAttack;

import java.util.Collection;
import java.util.List;

@Repository
public interface PokemonAttackDao extends JpaRepository<PokemonAttack, Long> {

    List<PokemonAttack> findAllByElementTypeIn(Collection<ElementType> elementTypeCollection);

}
