package pl.pokecounter.pokemon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;
import pl.pokecounter.pokemon.domain.LatestPokemonSearch;

import java.util.List;

@Repository
public interface LatestPokemonSearchDao extends JpaRepository<LatestPokemonSearch, Long> {

    @Query(value = "SELECT * FROM (SELECT  ID, ip_address, searched_at, pokemon_id, ROW_NUMBER() " +
            "OVER(PARTITION BY pokemon_id ORDER BY searched_at DESC) rn FROM latest_pokemon_search) a " +
            "WHERE rn = 1 ORDER BY searched_at DESC LIMIT 4",
            nativeQuery = true)
    List<LatestPokemonSearch>  findDistinctFour();

}
