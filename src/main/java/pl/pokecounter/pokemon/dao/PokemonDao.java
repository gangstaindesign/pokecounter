package pl.pokecounter.pokemon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.pokemon.domain.Pokemon;

import java.util.List;
import java.util.Optional;

@Repository
public interface PokemonDao extends JpaRepository<Pokemon, Long> {

    Iterable<Pokemon> findAllByOrderByNameAsc();
    List<Pokemon> findByNameContainingIgnoreCase(String name);
    Optional<Pokemon> findOneByNameIgnoreCase(String name);
}
