package pl.pokecounter.pokemon.dao;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.PopularPokemonSearch;

import java.util.List;
import java.util.Optional;

@Repository
public interface PopularPokemonSearchDao extends JpaRepository<PopularPokemonSearch, Long> {

    Optional<PopularPokemonSearch> findOneByPokemon(Pokemon pokemon);
    List<PopularPokemonSearch> findTop4ByOrderByTicksDesc();
}
