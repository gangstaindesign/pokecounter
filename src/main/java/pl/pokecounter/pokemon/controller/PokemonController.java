package pl.pokecounter.pokemon.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.dto.PokemonSuggestionDTO;
import pl.pokecounter.pokemon.service.ElementTypeService;
import pl.pokecounter.pokemon.service.PokemonService;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Optional;

@Controller
public class PokemonController {

    private static final String SUGGEST_URL = "/suggest";
    private static final String COUNTERS_BY_POKEMON_URL = "/counters/{name}";

    @Autowired private PokemonService pokemonService;
    @Autowired private ElementTypeService elementTypeService;

    @RequestMapping(SUGGEST_URL)
    @ResponseBody
    private List<PokemonSuggestionDTO> suggestByName(@RequestParam("pokemonName") String name) {
        return pokemonService.findAllByNameLike(name);
    }

    @RequestMapping(COUNTERS_BY_POKEMON_URL)
    private ModelAndView getAllCountersByPokemon(@PathVariable String name, HttpServletRequest httpServletRequest) {
        Optional<Pokemon> pokemonMaybe = pokemonService.findByName(name);

        if (!pokemonMaybe.isPresent()) {
            ModelAndView mav = new ModelAndView("redirect:/");
            mav.addObject("error", true);
            return mav;
        }

        Pokemon pokemon = pokemonMaybe.get();

        ModelAndView mav = new ModelAndView("front/counters");
        mav.addObject("pokemon", pokemon);
        mav.addObject("counterTypes", elementTypeService.getAllCounterTypesForPokemon(pokemon));
        mav.addObject("counters", pokemonService.findAllCountersForPokemon(pokemon, httpServletRequest));

        return mav;
    }



}
