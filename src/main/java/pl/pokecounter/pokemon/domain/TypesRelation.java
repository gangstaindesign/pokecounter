package pl.pokecounter.pokemon.domain;

import javax.persistence.*;

@Entity
@Table(name = "types_relation")
public class TypesRelation extends AbstractEntity {

    private static final long serialVersionUID = -5824904906214664372L;

    private ElementType attackingType;

    private ElementType defendingType;
    @Column(name="damage_multiplier", columnDefinition="Numeric(3,2)")
    private Double damageMultiplier;

    @ManyToOne
    @JoinColumn(name = "attacking_type_id")
    public ElementType getAttackingType() {
        return attackingType;
    }

    public void setAttackingType(ElementType attackingType) {
        this.attackingType = attackingType;
    }

    @ManyToOne
    @JoinColumn(name = "defending_type_id")
    public ElementType getDefendingType() {
        return defendingType;
    }

    public void setDefendingType(ElementType defendingType) {
        this.defendingType = defendingType;
    }

    public Double getDamageMultiplier() {
        return damageMultiplier;
    }

    public void setDamageMultiplier(Double damageMultiplier) {
        this.damageMultiplier = damageMultiplier;
    }
}
