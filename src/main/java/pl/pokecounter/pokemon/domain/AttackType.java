package pl.pokecounter.pokemon.domain;


public enum AttackType {
    NORMAL, SPECIAL
}
