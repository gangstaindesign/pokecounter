package pl.pokecounter.pokemon.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "pokemon")
public class Pokemon extends AbstractEntity {

    private static final long serialVersionUID = -8145111482409236164L;

    private String name;
    private String description;
    private String imageUrl;
    private String pokedexId;
    private int rarity;

    private List<ElementType> elementTypes;
    private List<PokemonAttack> pokemonAttacks;

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToMany(mappedBy = "pokemons")
    public List<ElementType> getElementTypes() {
        return elementTypes;
    }

    public void setElementTypes(List<ElementType> elementTypes) {
        this.elementTypes = elementTypes;
    }

    @OneToMany(mappedBy = "pokemonWithThisAttack",cascade = CascadeType.REMOVE)
    public List<PokemonAttack> getPokemonAttacks() {
        return pokemonAttacks;
    }

    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public String getPokedexId() {
        return pokedexId;
    }

    public void setPokedexId(String pokedexId) {
        this.pokedexId = pokedexId;
    }

    public void setPokemonAttacks(List<PokemonAttack> pokemonAttacks) {
        this.pokemonAttacks = pokemonAttacks;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }
}
