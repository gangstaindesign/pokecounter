package pl.pokecounter.pokemon.domain.dto;

import pl.pokecounter.pokemon.domain.Pokemon;

public class PokemonSuggestionDTO {

    private String value;
    private String data;

    public PokemonSuggestionDTO(Pokemon pokemon) {
        this.value = pokemon.getName();
        this.data = pokemon.getId() + "";
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }
}
