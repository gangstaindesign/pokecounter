package pl.pokecounter.pokemon.domain.dto;


public enum Effectiveness {
    INEFFECTIVE, VERY_INEFFECTIVE, NORMAL, EFFECTIVE, DOUBLE_EFFECTIVE;

    public static Effectiveness getEffectivnesForMultiplier(double damageMultiplier) {
        Effectiveness result;
        if(isBetween(damageMultiplier, 0, 0.75)) result = VERY_INEFFECTIVE;
                else if(isBetween(damageMultiplier, 0.75, 0.95)) result = INEFFECTIVE;
                else if(isBetween(damageMultiplier, 0.95, 1.05)) result = NORMAL;
                else if(isBetween(damageMultiplier, 1.05, 1.3)) result = EFFECTIVE;
                else if(isBetween(damageMultiplier, 1.3, 2.05)) result = DOUBLE_EFFECTIVE;
                else throw new IllegalArgumentException();
        return result;
    }

    public static boolean isBetween(double x, double lower, double upper) {
        return lower <= x && x < upper;
    }
}
