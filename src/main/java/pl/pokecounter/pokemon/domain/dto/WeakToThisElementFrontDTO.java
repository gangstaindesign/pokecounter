package pl.pokecounter.pokemon.domain.dto;

import pl.pokecounter.pokemon.domain.ElementType;

public class WeakToThisElementFrontDTO {

    public String name;
    public String color;

    public WeakToThisElementFrontDTO(ElementType elementType) {
        this.name = elementType.getName();
        this.color = elementType.getColor();
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
