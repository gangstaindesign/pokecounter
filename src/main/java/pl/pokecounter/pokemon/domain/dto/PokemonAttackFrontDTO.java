package pl.pokecounter.pokemon.domain.dto;

import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.PokemonAttack;

import java.util.List;

public class PokemonAttackFrontDTO {

    private String name;
    private String elementName;
    private boolean active;
    private String elementColor;

    public PokemonAttackFrontDTO(PokemonAttack pokemonAttack,  List<ElementType> vunerableToTypes) {
        this.name = pokemonAttack.getName();
        this.elementName = pokemonAttack.getElementType().getName();
        this.elementColor = pokemonAttack.getElementType().getColor();
        this.active = vunerableToTypes.contains(pokemonAttack.getElementType());
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getElementName() {
        return elementName;
    }

    public void setElementName(String elementName) {
        this.elementName = elementName;
    }

    public boolean isActive() {
        return active;
    }

    public void setActive(boolean active) {
        this.active = active;
    }

    public String getElementColor() {
        return elementColor;
    }

    public void setElementColor(String elementColor) {
        this.elementColor = elementColor;
    }
}
