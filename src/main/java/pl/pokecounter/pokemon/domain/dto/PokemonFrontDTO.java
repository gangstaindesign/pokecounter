package pl.pokecounter.pokemon.domain.dto;

import pl.pokecounter.pokemon.domain.AttackType;
import pl.pokecounter.pokemon.domain.ElementType;
import pl.pokecounter.pokemon.domain.Pokemon;

import java.util.List;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.toList;

public class PokemonFrontDTO {

    private Long id;
    private String name;
    private String imageUrl;
    private String countersUrl;
    private int rarity;
    private List<PokemonAttackFrontDTO> normalAttacks;
    private List<PokemonAttackFrontDTO> specialAttacks;

    public PokemonFrontDTO(Pokemon pokemon) {
        this.id = pokemon.getId();
        this.name = pokemon.getName();
        this.imageUrl = pokemon.getImageUrl();
        this.rarity = pokemon.getRarity();
    }

    public static PokemonFrontDTO mapToDTOWithAttacks(Pokemon pokemon, List<ElementType> vunerableToTypes) {
        PokemonFrontDTO pokemonFrontDTO = new PokemonFrontDTO(pokemon);

        pokemonFrontDTO.normalAttacks = pokemon.getPokemonAttacks()
                .stream()
                .filter(attack -> AttackType.NORMAL == attack.getAttackType())
                .map(attack -> new PokemonAttackFrontDTO(attack, vunerableToTypes))
                .sorted((a1, a2) -> Boolean.compare(a2.isActive(), a1.isActive()))
                .collect(toList());

        pokemonFrontDTO.specialAttacks = pokemon.getPokemonAttacks()
                .stream()
                .filter(attack -> AttackType.SPECIAL == attack.getAttackType())
                .map(attack -> new PokemonAttackFrontDTO(attack, vunerableToTypes))
                .sorted((a1, a2) -> Boolean.compare(a2.isActive(), a1.isActive()))
                .collect(toList());
        return pokemonFrontDTO;
    }

    public double getAttacksToTypesFittingAttacksRatio(List<ElementType> types){
        long attacksCount = Stream.concat(normalAttacks.stream(), specialAttacks.stream()).count();
        long fittingAttacksCount = 0;
        for (ElementType type : types) {
            long attacksFittngNumber = Stream.concat(normalAttacks.stream(), specialAttacks.stream())
                    .filter(atk -> atk.getElementName().equals(type.getName()))
                    .count();
            fittingAttacksCount += attacksFittngNumber;
        }
        return (double)fittingAttacksCount/attacksCount;
    }

    public double getActiveAttacksRatio(){
        double normalAttackRatio = calculateActiveAttacksRatioFor(normalAttacks);
        double specialAttackRatio = calculateActiveAttacksRatioFor(specialAttacks);
        /* coby nie zerowalo zupelnie drugiego ratio przy mnozeniu */
        if(normalAttackRatio == 0) normalAttackRatio = 0.001;
        if(specialAttackRatio == 0) specialAttackRatio = 0.001;
        return normalAttackRatio * specialAttackRatio;
    }

    private double calculateActiveAttacksRatioFor(List<PokemonAttackFrontDTO> attacks){
        int attacksCount = 0;
        int activeAttacks = 0;
        for (PokemonAttackFrontDTO attack : attacks) {
            attacksCount++;
            if (attack.isActive()) {
                activeAttacks++;
            }
        }
        return (double)activeAttacks/attacksCount;
    }

    public boolean isAtleastHalfAttacksCounters(){
        return getActiveAttacksRatio() > 0.24;
    }


    public int getRarity() {
        return rarity;
    }

    public void setRarity(int rarity) {
        this.rarity = rarity;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getImageUrl() {
        return imageUrl;
    }

    public void setImageUrl(String imageUrl) {
        this.imageUrl = imageUrl;
    }

    public List<PokemonAttackFrontDTO> getNormalAttacks() {
        return normalAttacks;
    }

    public void setNormalAttacks(List<PokemonAttackFrontDTO> normalAttacks) {
        this.normalAttacks = normalAttacks;
    }

    public List<PokemonAttackFrontDTO> getSpecialAttacks() {
        return specialAttacks;
    }

    public void setSpecialAttacks(List<PokemonAttackFrontDTO> specialAttacks) {
        this.specialAttacks = specialAttacks;
    }

    public String getCountersUrl() {
        return countersUrl;
    }

    public void setCountersUrl(String countersUrl) {
        this.countersUrl = countersUrl;
    }


}
