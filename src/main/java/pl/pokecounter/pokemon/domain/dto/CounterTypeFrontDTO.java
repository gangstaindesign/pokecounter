package pl.pokecounter.pokemon.domain.dto;


import pl.pokecounter.pokemon.domain.ElementType;

public class CounterTypeFrontDTO {
    public String name;
    public String color;
    public Effectiveness effectiveness;

    public CounterTypeFrontDTO(ElementType elementType, double damageMultiplier) {
        this.name = elementType.getName();
        this.color = elementType.getColor();
        effectiveness = Effectiveness.getEffectivnesForMultiplier(damageMultiplier);
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }

    public Effectiveness getEffectiveness() {
        return effectiveness;
    }

    public void setEffectiveness(Effectiveness effectiveness) {
        this.effectiveness = effectiveness;
    }

}
