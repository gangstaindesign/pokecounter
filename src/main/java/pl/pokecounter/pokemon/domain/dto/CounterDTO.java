package pl.pokecounter.pokemon.domain.dto;


import pl.pokecounter.pokemon.domain.ElementType;

public class CounterDTO {

    public ElementType elementType;
    public Effectiveness effectiveness;

    public CounterDTO(ElementType elementType, double damageMultiplier) {
        this.elementType = elementType;
        effectiveness = Effectiveness.getEffectivnesForMultiplier(damageMultiplier);
    }

    public ElementType getElementType() {
        return elementType;
    }

    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }

    public Effectiveness getEffectiveness() {
        return effectiveness;
    }

    public void setEffectiveness(Effectiveness effectiveness) {
        this.effectiveness = effectiveness;
    }
}
