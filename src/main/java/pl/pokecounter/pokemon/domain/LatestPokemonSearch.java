package pl.pokecounter.pokemon.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;
import java.util.Date;

@Entity
@Table(name = "latest_pokemon_search")
public class LatestPokemonSearch extends AbstractEntity {

    private static final long serialVersionUID = -4993065513789911527L;

    private Pokemon pokemon;
    private Date searchedAt;
    private String ipAddress;

    @ManyToOne
    @JoinColumn(name = "pokemon_id")
    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public Date getSearchedAt() {
        return searchedAt;
    }

    public void setSearchedAt(Date searchedAt) {
        this.searchedAt = searchedAt;
    }

    public String getIpAddress() {
        return ipAddress;
    }

    public void setIpAddress(String ipAddress) {
        this.ipAddress = ipAddress;
    }
}
