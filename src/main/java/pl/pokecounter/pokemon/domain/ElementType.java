package pl.pokecounter.pokemon.domain;

import javax.persistence.*;
import java.util.List;

@Entity
@Table(name = "element_type")
public class ElementType extends AbstractEntity {

    private static final long serialVersionUID = -4250647470883484132L;

    private String name;
    private String color;

    private List<Pokemon> pokemons;
    private List<TypesRelation> typesRelations;

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public void setPokemons(List<Pokemon> pokemons) {
        this.pokemons = pokemons;
    }

    @OneToMany(mappedBy = "defendingType", cascade = CascadeType.REMOVE)
    public List<TypesRelation> getTypesRelations() {
        return typesRelations;
    }

    @ManyToMany
    @JoinTable(
            name = "pokemon_to_element_type",
            joinColumns = @JoinColumn(name = "element_id", referencedColumnName = "id"),
            inverseJoinColumns = @JoinColumn(name = "pokemon_id", referencedColumnName = "id")
    )
    public List<Pokemon> getPokemons() {
        return pokemons;
    }

    public void setTypesRelations(List<TypesRelation> typesRelations) {
        this.typesRelations = typesRelations;
    }

    @Column
    public String getColor() {
        return color;
    }

    public void setColor(String color) {
        this.color = color;
    }
}
