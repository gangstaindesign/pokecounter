package pl.pokecounter.pokemon.domain;

import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

@Entity
@Table(name = "popular_pokemon_search")
public class PopularPokemonSearch extends AbstractEntity {

    private static final long serialVersionUID = 5770128192624350663L;

    private Pokemon pokemon;
    private Long ticks;

    @ManyToOne
    @JoinColumn(name = "pokemon_id")
    public Pokemon getPokemon() {
        return pokemon;
    }

    public void setPokemon(Pokemon pokemon) {
        this.pokemon = pokemon;
    }

    public Long getTicks() {
        return ticks;
    }

    public void setTicks(Long ticks) {
        this.ticks = ticks;
    }
}
