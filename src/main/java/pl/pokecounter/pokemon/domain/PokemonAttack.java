package pl.pokecounter.pokemon.domain;

import javax.persistence.*;

@Entity
@Table(name = "pokemon_attack")
public class PokemonAttack extends AbstractEntity {

    private static final long serialVersionUID = 3385758758907667432L;

    private String name;
    private ElementType elementType;
    private Pokemon pokemonWithThisAttack;
    private AttackType attackType;

    @Enumerated(EnumType.STRING)
    public AttackType getAttackType() {
        return attackType;
    }

    public void setAttackType(AttackType attackType) {
        this.attackType = attackType;
    }

    @ManyToOne
    @JoinColumn(name = "pokemon_id")
    public Pokemon getPokemonWithThisAttack() {
        return pokemonWithThisAttack;
    }

    public void setPokemonWithThisAttack(Pokemon pokemonWithThisAttack) {
        this.pokemonWithThisAttack = pokemonWithThisAttack;
    }

    @Column
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @ManyToOne
    @JoinColumn(name = "element_id")
    public ElementType getElementType() {
        return elementType;
    }

    public void setElementType(ElementType elementType) {
        this.elementType = elementType;
    }
}
