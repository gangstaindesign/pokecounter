package pl.pokecounter.pokemon.service;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.ArgumentCaptor;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import pl.pokecounter.pokemon.dao.PopularPokemonSearchDao;
import pl.pokecounter.pokemon.domain.Pokemon;
import pl.pokecounter.pokemon.domain.PopularPokemonSearch;

import java.util.Optional;

import static org.assertj.core.api.Assertions.assertThat;
import static org.mockito.AdditionalAnswers.returnsFirstArg;
import static org.mockito.BDDMockito.given;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;

@RunWith(MockitoJUnitRunner.class)
public class PokemonSearchServiceTest {

    @InjectMocks private PokemonSearchService pokemonSearchService; //testowana klasa, do niej wstrzykujesz MOCKI
    @Mock private PopularPokemonSearchDao popularPokemonSearchDao;  //jako że testujesz tylko PokemonSearchService,
                                                                    //inne komponenty które autowirujesz albo z ktorych korzystasz
                                                                    //JEBIĄ CIĘ i uznajesz ze one dzialaja poprawnie
                                                                    //dlatego sterujesz nimi jak pionkami i wyjasniasz jak maja działać
                                                                    //zapraszam dalej
    /*
        mock to taki kolega Dudek ktorego wyjasniasz
        mowisz mu
        JESLI WYWOLANA JEST METODA eat() masz zwrocic obiekt typu Food
        JESLI WYWOLANA JEST METODA sleep() masz zrobic nic
        JESLI WYWOLANA JEST METODA die() uzyj prawdziwej metody <- tak, mozesz powiedziec mockowi zeby wywolal faktycznie
                                                                        metode z klasy Dudek

        i on jak pionek elegancko słucha i robi to co mu karzesz

     */

     /*
        //given / arrange

        //when / act

        //then / assert

        (ja w projekcie mam given/when/then niektorzy maja arrange/act/assert
        to taka konwencja ze w
        //given przygotowujesz przykład testowy, obiekty, mowisz co ma sie dziac
        //when wywolujesz faktyczna metode na prawdziwej klasie którą testujesz
        //then upewniasz sie ze wszystko sie stało tak jak miało się stać

     */

    //przypadek pierwszy - nie ma jeszcze licznika dla danego pokemona
    @Test
    public void shouldCreateANewEntryIfPopularPokemonSearchDoesNotExistForAPokemon() throws Exception {
        //given
        final Pokemon fakePokemon = new Pokemon();
        given(popularPokemonSearchDao.findOneByPokemon(fakePokemon)).willReturn(Optional.empty());  //kazemy dao stworzyc sytuacje
                                                                                                     //w ktorej licznik nie istnieje
        given(popularPokemonSearchDao.saveAndFlush(any())).willAnswer(returnsFirstArg());   //tutaj nas jebie co sie stanie bo nie chcemy
                                                                                            // zapisac tego w bazie wiec robimy mini sztuczke xD

        final ArgumentCaptor<PopularPokemonSearch> captor = ArgumentCaptor.forClass(PopularPokemonSearch.class); //tego ziomka zrobilismy zeby zlapal nam argument
                                                                                                                //w popularPokemonSearchDao bo akurat jest chujowy przyklad xD
                                                                                                                // sory, tego na ogol nie robisz
        //when
        pokemonSearchService.addNewTick(fakePokemon);

        //then
        verify(popularPokemonSearchDao, times(1)).findOneByPokemon(fakePokemon); //upewnij sie ze byla wywolana metoda findOneByPokemon na mocku z konkretnym naszym pokemonem
        verify(popularPokemonSearchDao, times(1)).saveAndFlush(captor.capture()); //upewnij sie ze wywolales raz saveAndFlush i zlap argument

        final PopularPokemonSearch savedPopularPokemonSearch = captor.getValue();
        assertThat(savedPopularPokemonSearch.getPokemon()).isEqualTo(fakePokemon);
        assertThat(savedPopularPokemonSearch.getTicks()).isEqualTo(1L);
    }

    @Test
    public void shouldIncrementTicksByOneIfPopularPokemonSearchExists() throws Exception {
        //given
        final Pokemon fakePokemon = new Pokemon();
        final PopularPokemonSearch popularPokemonSearch = new PopularPokemonSearch();
        popularPokemonSearch.setTicks(69L);
        popularPokemonSearch.setPokemon(fakePokemon);

        given(popularPokemonSearchDao.findOneByPokemon(fakePokemon)).willReturn(Optional.of(popularPokemonSearch));
        given(popularPokemonSearchDao.saveAndFlush(any())).willAnswer(returnsFirstArg());

        final ArgumentCaptor<PopularPokemonSearch> captor = ArgumentCaptor.forClass(PopularPokemonSearch.class);

        //when
        pokemonSearchService.addNewTick(fakePokemon);

        //then
        verify(popularPokemonSearchDao, times(1)).findOneByPokemon(fakePokemon);
        verify(popularPokemonSearchDao, times(1)).saveAndFlush(captor.capture());

        final PopularPokemonSearch savedPopularPokemonSearch = captor.getValue();
        assertThat(savedPopularPokemonSearch).isNotNull();
        assertThat(savedPopularPokemonSearch.getPokemon()).isEqualTo(fakePokemon);
        assertThat(savedPopularPokemonSearch.getTicks()).isEqualTo(70L); //upewnij sie ze inkrementnelo
        assertThat(savedPopularPokemonSearch).isSameAs(popularPokemonSearch); //i ze zapisujesz ten sam obiekt, a nie nowy

    }
    /*
        przetestowalismy wszystkie przypadki(aż dwa xD) metody addNewTick, czy zachowuje sie tak jak powinna
        jak ladnie widac, nie potrzebowalismy ani bazy - bo nie interesuje nas zapis tego w bazie
                                              ani konkretnej implementacji dao - bo zrobilismy z niej mocka i wyjasnialismy co ma robic
        w innym tescie np zamockowalibysmy inny serwis i rowniez mowili mu co ma robic -
        BO TESTUJEMY TYLKO TEN KONKRETNY JEDEN SERWIS I RZECZY OD KTORYCH ON ZALEZY NAS J E B I Ą 

     */


}